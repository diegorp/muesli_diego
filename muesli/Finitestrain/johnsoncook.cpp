/****************************************************************************
 *
 *                                 M U E S L I   v 1.5
 *
 *
 *     Copyright 2016 IMDEA Materials Institute, Getafe, Madrid, Spain
 *     Contact: muesli.materials@imdea.org
 *     Author: Ignacio Romero (ignacio.romero@imdea.org)
 *
 *     This file is part of MUESLI.
 *
 *     MUESLI is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MUESLI is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/


#include "johnsoncook.h"
#include <string.h>
#include <cmath>

//Tolerances can be decreased in case convergence is affected
#define J2TOL1     1e-10
#define J2TOL2     1e-10
#define GAMMAITER1 10
#define GAMMAITER2 100
#define SQ23      0.816496580927726

using namespace std;
using namespace muesli;


johnsonCookMaterial::johnsonCookMaterial(const std::string& name,
                                           const materialProperties& cl)
:
finiteStrainMaterial(name, cl),
E(0.0), nu(0.0), lambda(0.0), mu(0.0), bulk(0.0), rho(1.0),
cp(0.0), cs(0.0), _A(0.0), _B(0.0), _C(0.0), _M(0.0), _N(0.0),
_edot0(1.0), _curT(1.0), _refT(1.0), _meltT(1.0), _D1(0.0),
_D2(0.0), _D3(0.0), _D4(0.0), _D5(0.0), _Dmax(0.99)
{
    muesli::assignValue(cl, "young",       E);
    muesli::assignValue(cl, "poisson",     nu);
    muesli::assignValue(cl, "lambda",      lambda);
    muesli::assignValue(cl, "mu",          mu);
    muesli::assignValue(cl, "density",     rho);
    muesli::assignValue(cl, "a",           _A);
    muesli::assignValue(cl, "b",           _B);
    muesli::assignValue(cl, "c",           _C);
    muesli::assignValue(cl, "m",           _M);
    muesli::assignValue(cl, "n",           _N);
    muesli::assignValue(cl, "edot0",       _edot0);
    muesli::assignValue(cl, "reftemp",     _refT);
    muesli::assignValue(cl, "temp",        _curT);
    muesli::assignValue(cl, "melttemp",    _meltT);
    muesli::assignValue(cl, "d1",          _D1);
    muesli::assignValue(cl, "d2",          _D2);
    muesli::assignValue(cl, "d3",          _D3);
    muesli::assignValue(cl, "d4",          _D4);
    muesli::assignValue(cl, "d5",          _D5);
    muesli::assignValue(cl, "dmax",        _Dmax);
    if ( cl.find("deletion") != cl.end() ) deletion = true;
    
    
    // E and nu have priority. If they are defined, define lambda and mu
    if (E*E > 0.0)
    {
        lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
        mu     = E/2.0/(1.0+nu);
    }
    else
    {
        nu = 0.5 * lambda / (lambda+mu);
        E  = mu*2.0*(1.0+nu);
    }
    
    // We set all the constants, so that later on all of them can be recovered fast
    bulk = lambda + 2.0/3.0 * mu;
    
    if (rho > 0.0)
    {
        cp = sqrt((lambda+2.0*mu)/rho);
        cs = sqrt(mu/rho);
    }
    
    if (_D1!=0.0 || _D2!=0.0 || _D3!=0.0 || _D4!=0.0 || _D5!=0.0)
    {
        damageModel = true;
    }
    else
    {
        damageModel = false;
    }
}




// Alternative matrial cretion method, for MP individual tests
johnsonCookMaterial::johnsonCookMaterial(const std::string& name,
                                           const double xE, const double xnu,
                                           const double xrho,   const double x_A,
                                           const double x_B, const double x_C,
                                           const double x_M, const double x_N,
                                           const double x_edot0, const double x_curT,
                                           const double x_refT, const double x_meltT)
:
finiteStrainMaterial(name),
E(xE), nu(xnu), lambda(0.0), mu(0.0), bulk(0.0), rho(xrho),
cp(0.0), cs(0.0),_A(x_A), _B(x_B), _C(x_C), _M(x_M), _N(x_N),
_edot0(x_edot0),_curT(x_curT), _refT(x_refT), _meltT(x_meltT), _D1(0.0),
_D2(0.0), _D3(0.0), _D4(0.0), _D5(0.0), _Dmax(0.99)
{
    // E and nu have priority. If they are defined, define lambda and mu
    if (E*E > 0.0)
    {
        lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
        mu     = E/2.0/(1.0+nu);
    }
    
    // We set all the constants, so that later on all of them can be recovered fast
    bulk = lambda + 2.0/3.0 * mu;
    
    if (rho > 0.0)
    {
        cp = sqrt((lambda+2.0*mu)/rho);
        cs = sqrt(mu/rho);
    }
    
    if (_D1!=0.0 || _D2!=0.0 || _D3!=0.0 || _D4!=0.0 || _D5!=0.0)
    {
        damageModel = true;
    }
    else
    {
        damageModel = false;
    }
}




johnsonCookMaterial::johnsonCookMaterial(const std::string& name,
                                           const double xE, const double xnu,
                                           const double xrho,   const double x_A,
                                           const double x_B, const double x_C,
                                           const double x_M, const double x_N,
                                           const double x_edot0, const double x_curT,
                                           const double x_refT, const double x_meltT,
                                           const double x_D1, const double x_D2,
                                           const double x_D3, const double x_D4,
                                           const double x_D5)
:
finiteStrainMaterial(name),
E(xE), nu(xnu), lambda(0.0), mu(0.0), bulk(0.0), rho(xrho),
cp(0.0), cs(0.0),_A(x_A), _B(x_B), _C(x_C), _M(x_M), _N(x_N),
_edot0(x_edot0),_curT(x_curT), _refT(x_refT), _meltT(x_meltT), _D1(x_D1),
_D2(x_D2), _D3(x_D3), _D4(x_D4), _D5(x_D5), _Dmax(0.99)
{
    // E and nu have priority. If they are defined, define lambda and mu
    if (E*E > 0.0)
    {
        lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
        mu     = E/2.0/(1.0+nu);
    }
    
    // We set all the constants, so that later on all of them can be recovered fast
    bulk = lambda + 2.0/3.0 * mu;
    
    if (rho > 0.0)
    {
        cp = sqrt((lambda+2.0*mu)/rho);
        cs = sqrt(mu/rho);
    }
    
    if (_D1!=0.0 || _D2!=0.0 || _D3!=0.0 || _D4!=0.0 || _D5!=0.0)
    {
        damageModel = true;
    }
    else
    {
        damageModel = false;
    }
}




double johnsonCookMaterial::characteristicStiffness() const
{
    return E;
}




bool johnsonCookMaterial::check() const
{
    if (mu > 0.0 && lambda+2.0*mu > 0.0) return true;
    else return false;
}




muesli::finiteStrainMP* johnsonCookMaterial::createMaterialPoint() const
{
    muesli::finiteStrainMP *mp = new johnsonCookMP(*this);
    return mp;
}




double johnsonCookMaterial :: density() const
{
    return rho;
}




// This function is much faster than the one with string property names, since it
// avoids string comparisons. It should be used.
double johnsonCookMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;
    
    switch (p)
    {
        case PR_LAMBDA:     ret = lambda;   break;
        case PR_MU:         ret = mu;       break;
        case PR_YOUNG:      ret = E;        break;
        case PR_POISSON:    ret = nu;       break;
        case PR_BULK:       ret = bulk;     break;
        case PR_CP:         ret = cp;       break;
        case PR_CS:         ret = cs;       break;
            
        default:
            std::cout << "\n Error in elasticIsotropicMaterial. Property not defined";
    }
    return ret;
}




void johnsonCookMaterial::print(std::ostream &of) const
{
    if (damageModel)
    {
        of  << "\n Johnson - Cook rate- and temperature-dependent plasticity with Damage option ";
    }
    else
    {
        of  << "\n Johnson - Cook rate- and temperature-dependent plasticity ";
    }
    of << "\n   Young modulus:  E   : " << E
    << "\n   Poisson ratio:  nu     : " << nu
    << "\n   Lame constants: Lambda : " << lambda
    << "\n                   Mu     : " << mu
    << "\n   Bulk modulus:   K      : " << bulk
    << "\n   Density                : " << rho;
    if (rho > 0.0)
    {
        of  << "\n   Wave velocities C_p    : " << cp;
        of  << "\n                   C_s    : " << cs;
    }
    if (damageModel)
    {
        of  << "\n   The yield Kirchhoff stress is of the form:"
        << "\n    |tau| - (1-D) sqrt(2/3) (A+B e^N) (1+C ln (edot/edot0))(1-theta^M)"
        << "\n    theta = (T - T0)/(Tm - T0)";
    }
    else
    {
        of  << "\n   The yield Kirchhoff stress is of the form:"
        << "\n    |tau| - sqrt(2/3) (A+B e^N) (1+C ln (edot/edot0))(1-theta^M)"
        << "\n    theta = (T - T0)/(Tm - T0)";
    }
    
    of  << "\n   A                  : " << _A
    << "\n   B                      : " << _B
    << "\n   C                      : " << _C
    << "\n   N                      : " << _N
    << "\n   dot{eps}_0             : " << _edot0
    << "\n   T                      : " << _curT
    << "\n   T0                     : " << _refT
    << "\n   Tm                     : " << _meltT
    << "\n   M                      : " << _M;
    if (damageModel)
    {
        of  <<"\n  D1                : " << _D1
        <<"\n  D2                    : " << _D2
        <<"\n  D3                    : " << _D3
        <<"\n  D4                    : " << _D4
        <<"\n  D5                    : " << _D5
        <<"\n  Dmax                  : " << _Dmax;
    }
}




void johnsonCookMaterial::setRandom()
{
    E      = muesli::randomUniform(1.0, 10.0);
    nu     = muesli::randomUniform(0.05, 0.45);
    rho    = muesli::randomUniform(1.0, 100.0);
    _A     = muesli::randomUniform(1.0, 10.0);
    _B     = muesli::randomUniform(1.0, 10.0);
    _C     = muesli::randomUniform(1.0, 10.0);
    _M     = muesli::randomUniform(1.0, 10.0);
    _N     = muesli::randomUniform(1.0, 10.0);
    _edot0 = muesli::randomUniform(1.0, 10.0);
    _meltT = muesli::randomUniform(100.0, 200.0);
    _curT  = muesli::randomUniform(60.0, 80.0);
    _refT  = muesli::randomUniform(60.0, 80.0);
    if (damageModel)
    {
        _D1    = muesli::randomUniform(0.0, 10.0);
        _D2    = muesli::randomUniform(0.0, 10.0);
        _D3    = muesli::randomUniform(0.0, 10.0);
        _D4    = muesli::randomUniform(0.0, 10.0);
        _D5    = muesli::randomUniform(0.0, 10.0);
        _Dmax  = muesli::randomUniform(0.0, 1.0);
    }
    
    lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
    mu     = E/2.0/(1.0+nu);
    cp     = sqrt((lambda+2.0*mu)/rho);
    cs     = sqrt(2.0*mu/rho);
    bulk   = lambda + 2.0/3.0 * mu;
}




bool johnsonCookMaterial::test(std::ostream &of)
{
    bool isok=true;
    setRandom();
    
    muesli::finiteStrainMP* p = this->createMaterialPoint();
    p->setRandom();
    
    isok = p->testImplementation(of);
    delete p;
    return isok;
}




double johnsonCookMaterial::waveVelocity() const
{
    return cp;
}




johnsonCookMP::johnsonCookMP(const johnsonCookMaterial &m)
:
finiteStrainMP(m),
theElastoplasticMaterial(m),
iso_n(0.0), iso_c(0.0),
epdot_n(0.0), epdot_c(0.0),
dgamma(0.0)
{
    be_n = be_c = istensor::identity();
    tau.setZero();
    lambda2 = lambda2TR = ivector(1.0, 1.0, 1.0);
    tau.setZero();
    nubarTR.setZero();
    nn[0] = ivector(1.0, 0.0, 0.0);
    nn[1] = ivector(0.0, 1.0, 0.0);
    nn[2] = ivector(0.0, 0.0, 1.0);
}




// Brent method function, as backup for Newton-Raphson, in case it fails
double johnsonCookMP::brentroot(double a, double b, double Ga,
                                  double Gb, double eqpn, double ntbar,
                                  double mu, double A,double B,
                                  double C, double N,double edot0,
                                  double dt, double tempterm)
{
    double s(0.0);
    double d(0.0);
    size_t count=1;
    
    if (fabs(Ga) < fabs(Gb))
    {
        double ch=a;
        a=b; b=ch;
        double Gch=Ga;
        Ga=Gb; Gb=Gch;
    }
    
    double c = a;
    double Gs = Gb;
    double Gc = Ga;
    int flag=1;
    
    
    while( !(Gs == 0.0 ) && !((fabs(Ga) < J2TOL2) && (fabs(Gb) < J2TOL2))  && count < GAMMAITER2)
    {
        if((Ga != Gc) && (Gb != Gc))
        {
            s=((a*Gb*Gc/((Ga-Gb)*(Ga-Gc)))+(b*Ga*Gc/((Gb-Ga)*(Gb-Gc)))+(c*Ga*Gb/((Gc-Ga)*(Gc-Gb))));
        }
        else
        {
            s=b-(Gb*((b-a)/(Gb-Ga)));
        }
        
        if(!(((3.0*a+b)/4.0)<s<b) || ((flag==1) && (fabs(s-b)>=fabs((b-c)/2.0))) || ((flag==0) && (fabs(s-b)>= fabs((c-d)/2.0))) || ((flag==1) && (fabs(b-c)< J2TOL2)) || ((flag==0) && (fabs(c-d)< J2TOL2)))
        {
            s=(a+b)/2.0;
            flag=1;
        }
        else
        {
            flag=0;
        }
        
        plasticReturnResidual(mu, A, B, C, N, edot0, eqpn, tempterm, ntbar, dt, s, Gs);
        d=c;
        c=b;
        Gc=Gb;
        
        if(Ga*Gs < 0.0)
        {
            b=s;
            Gb=Gs;
        }
        else
        {
            a=s;
            Ga=Gs;
        }
        
        if (fabs(Ga) < fabs(Gb))
        {
            double cp=a;
            a=b; b=cp;
            double Gcp=Ga;
            Ga=Gb; Gb=Gcp;
        }
        
        count++;
        
    }
    
    if (fabs(Ga) > J2TOL2 || fabs(Gb) > J2TOL2)
    {
        return b;
    }
    return s;
}




void johnsonCookMP::CauchyStress(istensor& sigma) const
{
    const double iJ = 1.0/Jc;
    
    // Reconstruct Cauchy stress
    sigma.setZero();
    for (size_t i=0; i<3; i++)
    {
        sigma.addScaledVdyadicV(iJ * tau[i], nn[i]);
    }
}




void johnsonCookMP::commitCurrentState()
{
    finiteStrainMP::commitCurrentState();
    
    be_n    = be_c;
    iso_n   = iso_c;
    epdot_n = epdot_c;
}




void johnsonCookMP::contractWithConvectedTangent(const ivector& V1, const ivector& V2, itensor& T) const
{
    itensor4 c;
    convectedTangent(c);
    
    T.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    T(i,k) += c(i,j,k,l)*V1(j)*V2(l);
                }
}




// Calls generic FiniteStrain numerical convected tangent
void johnsonCookMP::convectedTangent(itensor4& C) const
{
    this->numericalConvectedTangent(C);
}




void johnsonCookMP::convectedTangentTimesSymmetricTensor(const istensor& M,istensor& CM) const
{
    itensor4 C;
    convectedTangent(C);
    
    CM.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    CM(i,j) += C(i,j,k,l)*M(k,l);
                }
}




void johnsonCookMP::spatialTangent(itensor4& C) const
{    
    C.setZero();
    
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    if (i==j && k==l) C(i,j,k,l) += theElastoplasticMaterial.lambda;
                    if (i==k && j==l) C(i,j,k,l) += theElastoplasticMaterial.mu;
                    if (i==l && j==k) C(i,j,k,l) += theElastoplasticMaterial.mu;
                }
}




// Calculation of damage variable for each timestep
double johnsonCookMP::damageCalc(double thet, ivector vone, double eps, double epsdot)
{
    const double D1         = theElastoplasticMaterial._D1;
    const double D2         = theElastoplasticMaterial._D2;
    const double D3         = theElastoplasticMaterial._D3;
    const double D4         = theElastoplasticMaterial._D4;
    const double D5         = theElastoplasticMaterial._D5;
    const double Tc         = theElastoplasticMaterial._curT;
    const double T0         = theElastoplasticMaterial._refT;
    const double Tm         = theElastoplasticMaterial._meltT;
    const double tempterm   = (Tc-T0)/(Tm-T0);
    const double edot0      = theElastoplasticMaterial._edot0;
    double epsf;
    istensor sigma;
    tau = tau/(1-Dn);
    CauchyStress(sigma);
    tau = (1-Dn)*tau;
    
    double hydro = (sigma(0,0)+sigma(1,1)+sigma(2,2))/3;
    double VMS=sqrt((((sigma(0,0)-sigma(1,1))*(sigma(0,0)-sigma(1,1))
                     +(sigma(1,1)-sigma(2,2))*(sigma(1,1)-sigma(2,2))
                     +(sigma(2,2)-sigma(0,0))*(sigma(2,2)-sigma(0,0)))/2)
                     +3*((sigma(0,1)*sigma(0,1))+(sigma(1,2)*sigma(1,2))+(sigma(2,0)*sigma(2,0))));
    triax = hydro/VMS;
    if (hydro==0.0 || std::isnan(triax))
    {
        triax = 0.0;
    }
    if (triax<1.5)
    {
        epsf = (D1+D2*exp(D3*triax))*(1.0+D4*log(epsdot/edot0))*(1.0+D5*tempterm);
    }
    else
    {
        epsf = (D1+D2*exp(D3*1.5))*(1.0+D4*log(epsdot/edot0))*(1.0+D5*tempterm);
    }
    double D = Dn+((iso_c-iso_n)/epsf);
    
    return D;
}




double johnsonCookMP::deviatoricEnergy() const
{
    return 0.0;
}




double johnsonCookMP::dissipatedEnergy() const
{
    const ivector vone(1.0, 1.0, 1.0);
    ivector taubar = tau - (1.0/3.0) * tau.dot(vone)*vone;
    
    double dissEnergy;
    if(tc-tn>0.0)
    {
        dissEnergy = dgamma*taubar.norm()/(tc-tn);
    }
    else
    {
        dissEnergy = 0.0;
    }
    
    return dissEnergy;
}




double johnsonCookMP::effectiveStoredEnergy() const
{
    double dt = tc - tn;
    return storedEnergy() + dt * dissipatedEnergy();
}




void johnsonCookMP::explicitRadialReturn(const ivector &taudev, double ep, double epdot)
{
    const double mu         = theElastoplasticMaterial.mu;
    const double A          = theElastoplasticMaterial._A;
    const double B          = theElastoplasticMaterial._B;
    const double C          = theElastoplasticMaterial._C;
    const double N          = theElastoplasticMaterial._N;
    const double M          = theElastoplasticMaterial._M;
    const double Tc         = theElastoplasticMaterial._curT;
    const double T0         = theElastoplasticMaterial._refT;
    const double Tm         = theElastoplasticMaterial._meltT;
    const double tempterm   = (1.0 - pow( (Tc-T0)/(Tm-T0) , M ));
    const double edot0      = theElastoplasticMaterial._edot0;
    
    double hard(1.0), dhard(1.0);
    if (ep > 0.0)
    {
        hard  = A + B*pow(ep,N);
        dhard = SQ23*B*N*pow(ep,N-1.0);
    }
    
    double rate(1.0), drate(1.0);
    if (epdot > 0.0)
    {
        rate  = 1.0 + C*log(epdot/edot0);
        drate = C/epdot;
    }
    
    double sigma_y = SQ23 * hard * rate * tempterm;
    dgamma = (taudev.norm() - sigma_y)/(2.0*mu);
    iso_c = ep + SQ23*dgamma*(dhard * rate + hard * drate)*tempterm;
}




double johnsonCookMP::kineticPotential() const
{
    return 0.0;
}




double johnsonCookMP::plasticSlip() const
{
    return iso_c;
}




// TaubarTR: trial deviatoric tau.
// This function finds dgamma root and equivalent plastic strain eqp in the current time step.
// Two-fold approach convergence algorithm, using in first place a NR scheme to find the root (dgamma)
// of G equation. In case this algorithm is not able (due to high derivative problems near the root
// associated to JC equation), the function jumps to a Brent scheme, whose convergence is assured.
void johnsonCookMP::plasticReturn(const ivector& taubarTR)
{
    const double mu         = theElastoplasticMaterial.mu;
    const double A          = theElastoplasticMaterial._A;
    const double B          = theElastoplasticMaterial._B;
    const double C          = theElastoplasticMaterial._C;
    const double M          = theElastoplasticMaterial._M;
    const double N          = theElastoplasticMaterial._N;
    const double edot0      = theElastoplasticMaterial._edot0;
    const double Tc         = theElastoplasticMaterial._curT;
    const double T0         = theElastoplasticMaterial._refT;
    const double Tm         = theElastoplasticMaterial._meltT;
    const double tempterm   = (1.0 - pow( (Tc-T0)/(Tm-T0) , M ));
    const double dt         = tc - tn;
    
    double eqpn = iso_n;
    double ntbar = taubarTR.norm();
    
    // The unknown "x" is delta-gamma
    double x = edot0*dt/1e2;
    double G, DG;
    int flagbrent   = 0;
    int flagbrent2  = 1;
    plasticReturnResidual(mu,A,B,C,N,edot0,eqpn,tempterm,ntbar,dt, x, G);
    
    
    // NR scheme. "x" represents the unknown delta-gamma
    size_t count = 0;
    while ( fabs(G) > (J2TOL1*A) && ++count < GAMMAITER1)
    {
        plasticReturnTangent(mu,A,B,C,N,edot0,eqpn, tempterm, ntbar, dt, x, DG);
        x -= G/DG;
        plasticReturnResidual(mu,A,B,C,N,edot0,eqpn, tempterm, ntbar, dt, x, G);
    }
    
    // The "if" condition tests whether the convergence with NR scheme has succeded or not, in which case
    // will set flagbrent to 1.
    if (count >= GAMMAITER1 || std::isnan(G) || x<=0.0)
    {
        flagbrent=1;
    }
    
    else
    {
        double x2 = dgamma;
        dgamma = x;
        iso_c  = iso_n + SQ23*dgamma;
        
        // In case a Nan is given during NR scheme, the previous value for dgamma is recovered
        if (std::isnan(dgamma))
        {
            dgamma = x2;
        }
    }
    
    // Initially, two points with different function values wihtin G must be given to the Brent method
    // to assure its convergence. Ga and Gb are both multiplied/divided until their sign is opposite.
    if(flagbrent==1)
    {
        eqpn = iso_n;
        ntbar = taubarTR.norm();
        double a=1e-12, b=1e-5;
        double Ga, Gb;
        
        plasticReturnResidual(mu,A,B,C,N,edot0,eqpn, tempterm, ntbar, dt, a, Ga);
        plasticReturnResidual(mu,A,B,C,N,edot0,eqpn, tempterm, ntbar, dt, b, Gb);
        
        while(Gb<0.0)
        {
            b=b*10.0;
            plasticReturnResidual(mu,A,B,C,N,edot0,eqpn, tempterm, ntbar, dt, b, Gb);
            if (b>2.0) break;
        }
        
        // The value of "a" can be very close to zero due to the JC equation features
        // and because of this the division the loop is stopped, making not a big difference compared
        // to the real value, given that it is really small.
        while(Ga>0.0)
        {
            a=a/15.0;
            plasticReturnResidual(mu,A,B,C,N,edot0,eqpn, tempterm, ntbar, dt, a, Ga);
            if (a<1e-50)
            {
                dgamma = 1e-50;
                flagbrent2 = 0;
                break;
            }
        }
        
        if(flagbrent == 1 && flagbrent2 == 1)
        {
            // Call to Brent method function in case NR fails and "a" value is high enough
            double x1 = dgamma;
            dgamma = brentroot(a, b, Ga, Gb, eqpn, ntbar, mu, A, B ,C ,N ,edot0, dt, tempterm);
            // In case a Nan is given during Brent scheme, the previous value for dgamma is recovered
            if (std::isnan(dgamma))
            {
                dgamma = x1;
            }
            iso_c  = iso_n + SQ23*dgamma;
        }
    }
}




// Calculation of G function value given each iteration value of dgamma
void johnsonCookMP::plasticReturnResidual(double mu, double A, double B, double C,
                                            double N, double edot0, double eqpn, double tempterm,
                                            double tau, double dt, double dgamma, double& G)
{
    double deqp = SQ23*dgamma;
    double eqp  = eqpn + deqp;
    
    double hard = A;
    if (eqp > 0.0)
    {
        hard += B*pow(eqp,N);
    }
    
    double rate = 1.0;
    if (deqp > 0.0 && dt > 0.0)
    {
        rate += C*log(deqp/(dt*edot0));
    }

    G = 2.0*mu*dgamma - tau + SQ23*hard*rate*tempterm;
}




// Calculation of G function derivative value given each iteration value of dgamma
void  johnsonCookMP::plasticReturnTangent(double mu, double A, double B, double C,
                                            double N, double edot0, double eqpn, double tempterm,
                                            double tau, double dt, double dgamma, double& DG)
{
    double deqp = SQ23*dgamma;
    double eqp  = eqpn + deqp;
    
    double hard = A, dhard = 0.0;
    if (eqp > 0.0)
    {
        hard += B*pow(eqp,N);
        dhard = SQ23*B*N*pow(eqp,N-1.0);
    }
    
    
    double rate = 1.0, drate = 0.0;
    if (deqp > 0.0 && dt > 0.0)
    {
        rate += C*log(deqp/(dt*edot0));
        drate = C/dgamma;
    }
    
    DG = 2.0*mu + SQ23*dhard*rate*tempterm + SQ23*hard*drate*tempterm;
}




void johnsonCookMP::resetCurrentState()
{
    finiteStrainMP::resetCurrentState();
    
    iso_c   = iso_n;
    be_c    = be_n;
    epdot_c = epdot_n;
}




void johnsonCookMP::setConvergedState(const double theTime, const itensor& F,
                                        const double iso, const ivector& kine,
                                        const istensor& be)
{
    tn     = theTime;
    Fn     = F;
    Jn     = Fn.determinant();
    iso_n  = iso;
    be_n   = be;
}




void johnsonCookMP::setRandom()
{
    iso_c = iso_n = muesli::randomUniform(1.0, 2.0);
    
    ivector tmp; tmp.setRandom();
    ivector vone(1.0, 1.0, 1.0);
    
    itensor Fe; Fe.setRandom();
    if (Fe.determinant() < 0.0) Fe *= -1.0;
    be_c = be_n = istensor::tensorTimesTensorTransposed(Fe);
    
    itensor Fp; Fp.setRandom();
    Fp *= 1.0/cbrt(Fp.determinant());
    Fc = Fn = Fe*Fp;
    Jc = Fc.determinant();
    if (theElastoplasticMaterial.damageModel)
    {
        Dc = muesli::randomUniform(0.0, 1.0);
    }
}




double johnsonCookMP::storedEnergy() const
{
    const double lambda = theElastoplasticMaterial.lambda;
    const double mu     = theElastoplasticMaterial.mu;
    
    // Logarithmic principal elastic stretches
    ivector lambda2, xeigvec[3];
    be_c.spectralDecomposition(xeigvec, lambda2);
    ivector epse;
    for (size_t i=0; i<3; i++)
    {
        epse[i] = 0.5*log(lambda2[i]);
    }
    
    const double We = 0.5*lambda*( epse(0) + epse(1) + epse(2) ) * ( epse(0) + epse(1) + epse(2) )
    + mu * epse.squaredNorm();
    double Wp = dissipatedEnergy()*0.1/0.9;
    return Wp+We;
}




void johnsonCookMP::updateCurrentState(const double theTime, const istensor& C)
{
    itensor U = istensor::squareRoot(C);
    this->updateCurrentState(theTime, U);
}




void johnsonCookMP::updateCurrentState(const double theTime, const itensor& F)
{
    finiteStrainMP::updateCurrentState(theTime, F);
    
    // Recover material parameters
    const double mu     = theElastoplasticMaterial.mu;
    const double kappa  = theElastoplasticMaterial.bulk;
    const ivector vone(1.0, 1.0, 1.0);
    int flagD = 1;
    int flagDc = 0;
    
    // Incremental deformation gradient f = F * Fn^-1 */
    itensor f = F * Fn.inverse();
    
    
    //---------------------------------------------------------------------------------------
    //                                     trial state
    //---------------------------------------------------------------------------------------
    // Trial elastic finger tensor b_e
    istensor beTR = istensor::FSFt(f, be_n);
    
    // Logarithmic principal elastic stretches

    beTR.spectralDecomposition(nn, lambda2TR);
    ivector epseTR;
    ivector devEpseTR;
    for (size_t i=0; i<3; i++)
    {
        epseTR[i] = 0.5*log(lambda2TR[i]);
    }
    double theta = epseTR.dot(vone);
    
    // Trial state with frozen plastic flow
    if (!theElastoplasticMaterial.damageModel || (theElastoplasticMaterial.damageModel && Dn!=1.0))
    {
        devEpseTR = epseTR - 1.0/3.0*theta*vone;
    }
    double  isoTR     = iso_n;
    double  DTR       = Dn;
    ivector tauVol    = kappa*theta*vone;
    ivector tauTR;
    
    // Trial deviatoric principal Kirchhoff stress -- Hencky model
    if (theElastoplasticMaterial.damageModel && Dn==1.0)
    {
        tauTR = tauVol;
        if(theta>0.0)
        {
            tauVol[0]=tauVol[1]=tauVol[2]=0.0;
        }
    }
    else
    {
        tauTR = tauVol + 2.0*mu*devEpseTR;
    }
    
    // Yield function value at trial state
    double phiTR;
    if(theElastoplasticMaterial.damageModel)
    {
        phiTR = yieldFunctionDMG(tauTR, isoTR, 0.0, DTR);
    }
    else
    {
        phiTR = yieldFunction(tauTR, isoTR, 0.0);
    }

    // Explicit computations, deactivated in this version
    //double edot0 = theElastoplasticMaterial._edot0;
    //double edot = (epdot_n < 1e-4*edot0)  ? 1e-4*edot0 : epdot_n;
    //double phiTR = yieldFunction(tauTR, iso_n, edot);
    
    //---------------------------------------------------------------------------------------
    //                   check trial state and radial return
    //---------------------------------------------------------------------------------------
    ivector devepse_c;
        
    if (phiTR <= 0.0)
    {
        // Elastic step: trial -> n+1
        iso_c     = isoTR;
        devepse_c = devEpseTR;
        dgamma    = 0.0;
        flagD = 0;
        if(theElastoplasticMaterial.damageModel)
        {
            Dc = DTR;
        }
    }
    
    else
    {
        if (!theElastoplasticMaterial.damageModel || Dn!=1.0)
        {
            // Deviatoric stress
            devTauTR = 2.0*mu*devEpseTR;
        
            // Plastic step : return mapping in principal stretches space,
            // solution goes into MP variables dgamma and iso_c
            plasticReturn(devTauTR);
        
            //explicitRadialReturn(devTauTR, iso_n, edot);
        
            // Correct the trial quantities
            nubarTR = devTauTR * (1.0/devTauTR.norm());
            devepse_c = devEpseTR - dgamma * nubarTR;
        }
    }
    
    // Update elastic finger tensor
    ivector epse_c;
    if (theElastoplasticMaterial.damageModel && Dn==1.0)
    {
        epse_c = 1.0/3.0*theta*vone;
    }
    else
    {
        epse_c = devepse_c + 1.0/3.0*theta*vone;
    }
    be_c.setZero();
    for (unsigned i=0; i<3; i++)
    {
        lambda2(i) = exp(2.0*epse_c[i]);
        be_c.addScaledVdyadicV( lambda2(i), nn[i] );
    }
    
    double theta_c = epse_c.dot(vone);
    
    // Update of Kirchhof stress and Damage variable, in case Damage model is in use
    // and the temporal step lies within the plastic regime deformation, considering
    // cases of Dmax = 1.0 or Dmax < 1.0, selected by user
    if(theElastoplasticMaterial.damageModel && Dn!=1.0)
    {
        if (theta_c>0.0)
        {
            tau = (1.0-Dn) * (tauVol + 2.0 * mu * devepse_c);
        }
        else
        {
            tau =  tauVol + (1.0-Dn) * 2.0 * mu * devepse_c;
        }
    }
    else if(theElastoplasticMaterial.damageModel && Dn==1.0)
    {
        if (theta_c>0.0)
        {
            tau[0]=tau[1]=tau[2]=0.0;
        }
        else
        {
            tau =  tauVol;
        }
    }
    else if (!theElastoplasticMaterial.damageModel)
    {
        tau = tauVol + 2.0 * mu * devepse_c;
    }
    
    double dt = tc - tn;
    if(dgamma>0.0)
    {
        epdot_c = dt > 0.0  ? SQ23*dgamma/dt : epdot_n;
    }
    else
    {
        epdot_c = dt > 0.0 ? SQ23*1e-50/dt : epdot_n;
    }
    
    
    // Recalculation of current damage variable in case it is possible that it increases.
    if(theElastoplasticMaterial.damageModel && flagD==1 && !fullyDamaged)
    {
        Dc = damageCalc(theta, vone, iso_c, epdot_c);
        if(Dc>theElastoplasticMaterial._Dmax)
        {
            Dc = theElastoplasticMaterial._Dmax;
            if (std::isnan(Dc))
            {
                Dc=Dn;
            }
            
            fullyDamaged = true;
            
            if (theta_c>0.0)
            {
                tau = (1.0-Dc) * (tauVol + 2.0 * mu * devepse_c);
            }
            else
            {
                tau =  tauVol + (1.0-Dc) * 2.0 * mu * devepse_c;
            }
            
            flagDc = 1;
        }
    }
        
    // Testing of yield function to test dgamma root solution with and without Damage model, DEACTIVATED by default
    /*if(theElastoplasticMaterial.damageModel)
    {
        if (flagDc == 0)
        {
            double kk = yieldFunctionDMG(tau, iso_c, epdot_c, Dn);
            if (fabs(kk)>0.1 && dgamma>0.0)
            {
                std::cout<<"\n"<<kk;
                std::cout<<"\n"<<dgamma;
                std::cout<<"\n"<<theTime<<"\n";
            }
        }
        if (flagDc == 1)
        {
            double kk = yieldFunctionDMG(tau, iso_c, epdot_c, Dc);
            if (fabs(kk)>0.1 && dgamma>0.0)
            {
                std::cout<<"\n"<<kk;
                std::cout<<"\n"<<dgamma;
                std::cout<<"\n"<<theTime<<"\n";
            }
        }
    }
    else
    {
        double kk = yieldFunction(tau, iso_c, epdot_c);
        if (fabs(kk)>0.1 && dgamma>0.0)
        {
            std::cout<<"\n"<<kk;
            std::cout<<"\n"<<dgamma;
            std::cout<<"\n"<<theTime<<"\n";
        }
    }*/
}




double johnsonCookMP::volumetricEnergy() const
{
    return 0.0;
}




// Yield function in principal Kirchhoff space
double johnsonCookMP::yieldFunction(const ivector& tau,
                                      const double&  eps,
                                      const double&  epsdot) const
{
    const double A          = theElastoplasticMaterial._A;
    const double B          = theElastoplasticMaterial._B;
    const double C          = theElastoplasticMaterial._C;
    const double N          = theElastoplasticMaterial._N;
    const double M          = theElastoplasticMaterial._M;
    const double Tc         = theElastoplasticMaterial._curT;
    const double T0         = theElastoplasticMaterial._refT;
    const double Tm         = theElastoplasticMaterial._meltT;
    const double tempterm   = (1.0 - pow( (Tc-T0)/(Tm-T0) , M ));
    
    const double  edot0 = theElastoplasticMaterial._edot0;
    const ivector vone(1.0, 1.0, 1.0);
    double sigma_y;
    
    double hard = A + B*pow(eps,N);
    double rate = (epsdot <= 0.0) ? 1.0 : 1.0 + C*log(epsdot/edot0);
    
    sigma_y = sqrt(2.0/3.0) * hard * rate * tempterm;
    ivector taubar = tau - (1.0/3.0) * tau.dot(vone)*vone;
    return  taubar.norm() - sigma_y;
}




// Yield function in principal Kirchhoff space, when Damage model is in use
double johnsonCookMP::yieldFunctionDMG(const ivector& tau,
                                      const double&  eps,
                                      const double&  epsdot,
                                      const double& D) const
{
    const double A          = theElastoplasticMaterial._A;
    const double B          = theElastoplasticMaterial._B;
    const double C          = theElastoplasticMaterial._C;
    const double N          = theElastoplasticMaterial._N;
    const double M          = theElastoplasticMaterial._M;
    const double Tc         = theElastoplasticMaterial._curT;
    const double T0         = theElastoplasticMaterial._refT;
    const double Tm         = theElastoplasticMaterial._meltT;
    const double tempterm   = (1.0 - pow( (Tc-T0)/(Tm-T0) , M ));
    
    const double  edot0 = theElastoplasticMaterial._edot0;
    const ivector vone(1.0, 1.0, 1.0);
    double sigma_y;
    
    double hard = A + B*pow(eps,N);
    double rate = (epsdot <= 0.0) ? 1.0 : 1.0 + C*log(epsdot/edot0);
    
    sigma_y = (1-D)*sqrt(2.0/3.0) * hard * rate * tempterm;
    ivector taubar = tau - (1.0/3.0) * tau.dot(vone)*vone;

    return  taubar.norm() - sigma_y;
}
