/****************************************************************************
 *
 *                                 M U E S L I   v 1.5
 *
 *
 *     Copyright 2018 IMDEA Materials Institute, Getafe, Madrid, Spain
 *     Contact: muesli.materials@imdea.org
 *     Authors: Ignacio Romero (ignacio.romero@imdea.org)/ Juan de Pablos (juanluis.pablos@imdea.org)
 *
 *     This file is part of MUESLI.
 *
 *     MUESLI is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MUESLI is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/


#include "zerilliarmstrong.h"
#include <string.h>
#include <cmath>
#include <stdio.h>
#include <iostream>

//Tolerances can be decreased in case convergence is affected
#define J2TOL1     1e-5
#define J2TOL2     1e-5
#define GAMMAITER1 10
#define GAMMAITER2 100
#define SQ23      0.816496580927726

using namespace std;
using namespace muesli;




zerilliArmstrongMaterial :: zerilliArmstrongMaterial(const std::string& name,
                                                     const materialProperties& cl)
:
finiteStrainMaterial(name, cl), E(0.0), nu(0.0), lambda(0.0), mu(0.0),
                                bulk(0.0), rho(1.0), cp(0.0), cs(0.0),
                                _C0 (0.0), _C1(0.0), _C2(0.0), _C3(0.0),
                                _C4(0.0), _C5(0.0), _n(0.0),_curT(1.0),
                                _refT(1.0), _meltT(1.0), _D1(0.0), _D2(0.0),
                                _D3(0.0), _D4(0.0), _D5(0.0), _Dmax(0.99),
                                _edot0(1.0)
{
    muesli::assignValue(cl, "young",          E);
    muesli::assignValue(cl, "poisson",       nu);
    muesli::assignValue(cl, "lambda",    lambda);
    muesli::assignValue(cl, "mu",            mu);
    muesli::assignValue(cl, "density",      rho);
    muesli::assignValue(cl, "c0",           _C0);
    muesli::assignValue(cl, "c1",           _C1);
    muesli::assignValue(cl, "c2",           _C2);
    muesli::assignValue(cl, "c3",           _C3);
    muesli::assignValue(cl, "c4",           _C4);
    muesli::assignValue(cl, "c5",           _C5);
    muesli::assignValue(cl, "n",             _n);
    muesli::assignValue(cl, "temp",       _curT);
    muesli::assignValue(cl, "reftemp",    _refT);
    muesli::assignValue(cl, "melttemp",  _meltT);
    muesli::assignValue(cl, "d1",           _D1);
    muesli::assignValue(cl, "d2",           _D2);
    muesli::assignValue(cl, "d3",           _D3);
    muesli::assignValue(cl, "d4",           _D4);
    muesli::assignValue(cl, "d5",           _D5);
    muesli::assignValue(cl, "dmax",       _Dmax);
    muesli::assignValue(cl, "edot0",     _edot0);
    if ( cl.find("deletion") != cl.end() ) deletion = true;
    
    
    // E and nu have priority. If they are defined, define lambda and mu
    if (E*E > 0.0)
    {
        lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
        mu     = E/2.0/(1.0+nu);
    }
    else
    {
        nu = 0.5 * lambda / (lambda+mu);
        E  = mu*2.0*(1.0+nu);
    }
    
    // We set all the constants, so that later on all of them can be recovered fast
    bulk = lambda + 2.0/3.0 * mu;
    
    if (rho > 0.0)
    {
        cp = sqrt((lambda+2.0*mu)/rho);
        cs = sqrt(mu/rho);
    }
    
    if (_D1!=0.0 || _D2!=0.0 || _D3!=0.0 || _D4!=0.0 || _D5!=0.0)
    {
        damageModel = true;
    }
    else
    {
        damageModel = false;
    }
}




zerilliArmstrongMaterial :: zerilliArmstrongMaterial(const std::string& name,
                                                     const double xE, const double xnu, const double xrho,
                                                     const double x_C0, const double x_C1, const double x_C2,
                                                     const double x_C3, const double x_C4, const double x_C5,
                                                     const double x_n, const double x_curT)
:
finiteStrainMaterial(name), E(xE), nu(xnu), lambda(0.0), mu(0.0),
                            bulk(0.0), rho(xrho), cp(0.0), cs(0.0),
                            _C0 (x_C0), _C1(x_C1), _C2(x_C2), _C3(x_C3),
                            _C4(x_C4), _C5(x_C5), _n(x_n),_curT(x_curT),
                            _refT(1.0), _meltT(0.0), _D1(0.0), _D2(0.0),
                            _D3(0.0), _D4(0.0), _D5(0.0), _Dmax(0.99),
                            _edot0(0.0)
{
    // E and nu have priority. If they are defined, define lambda and mu
    if (E*E > 0.0)
    {
        lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
        mu     = E/2.0/(1.0+nu);
    }
    else
    {
        nu = 0.5 * lambda / (lambda+mu);
        E  = mu*2.0*(1.0+nu);
    }
    
    // We set all the constants, so that later on all of them can be recovered fast
    bulk = lambda + 2.0/3.0 * mu;
    
    if (rho > 0.0)
    {
        cp = sqrt((lambda+2.0*mu)/rho);
        cs = sqrt(mu/rho);
    }
    
    if (_D1!=0.0 || _D2!=0.0 || _D3!=0.0 || _D4!=0.0 || _D5!=0.0)
    {
        damageModel = true;
    }
    else
    {
        damageModel = false;
    }
}




zerilliArmstrongMaterial :: zerilliArmstrongMaterial(const std::string& name, const double xE, const double xnu,
                                                     const double xrho, const double x_C0, const double x_C1,
                                                     const double x_C2, const double x_C3, const double x_C4,
                                                     const double x_C5, const double x_n, const double x_curT,
                                                     const double x_refT, const double x_meltT, const double x_D1,
                                                     const double x_D2, const double x_D3, const double x_D4,
                                                     const double x_D5, const double x_edot0)
:
finiteStrainMaterial(name), E(xE), nu(xnu), lambda(0.0), mu(0.0),
                            bulk(0.0), rho(xrho), cp(0.0), cs(0.0),
                            _C0 (x_C0), _C1(x_C1), _C2(x_C2), _C3(x_C3),
                            _C4(x_C4), _C5(x_C5), _n(x_n),_curT(x_curT),
                            _refT(x_refT), _meltT(x_meltT), _D1(x_D1),
                            _D2(x_D2), _D3(x_D3), _D4(x_D4), _D5(x_D5),
                            _Dmax(0.99), _edot0(x_edot0)
{
    // E and nu have priority. If they are defined, define lambda and mu
    if (E*E > 0.0)
    {
        lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
        mu     = E/2.0/(1.0+nu);
    }
    else
    {
        nu = 0.5 * lambda / (lambda+mu);
        E  = mu*2.0*(1.0+nu);
    }
    
    // We set all the constants, so that later on all of them can be recovered fast
    bulk = lambda + 2.0/3.0 * mu;
    
    if (rho > 0.0)
    {
        cp = sqrt((lambda+2.0*mu)/rho);
        cs = sqrt(mu/rho);
    }
    
    if (_D1!=0.0 || _D2!=0.0 || _D3!=0.0 || _D4!=0.0 || _D5!=0.0)
    {
        damageModel = true;
    }
    else
    {
        damageModel = false;
    }
}




double zerilliArmstrongMaterial::characteristicStiffness() const
{
    return E;
}




bool zerilliArmstrongMaterial::check() const
{
    if (mu > 0.0 && lambda+2.0*mu > 0.0) return true;
    else return false;
}




muesli::finiteStrainMP* zerilliArmstrongMaterial::createMaterialPoint() const
{
    muesli::finiteStrainMP *mp = new zerilliArmstrongMP(*this);
    return mp;
}




double zerilliArmstrongMaterial::density() const
{
    return rho;
}




// This function is much faster than the one with string property names, because
// avoids string comparisons. It should be used.
double zerilliArmstrongMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;
    
    switch (p)
    {
        case PR_LAMBDA:     ret = lambda;   break;
        case PR_MU:         ret = mu;       break;
        case PR_YOUNG:      ret = E;        break;
        case PR_POISSON:    ret = nu;       break;
        case PR_BULK:       ret = bulk;     break;
        case PR_CP:         ret = cp;       break;
        case PR_CS:         ret = cs;       break;
            
        default:
            std::cout << "\n Error in elasticIsotropicMaterial. Property not defined";
    }
    return ret;
}




void zerilliArmstrongMaterial :: print(std::ostream &of) const
{
    if (damageModel)
    {
        of  << "\n Zerilli - Armstrong rate- and temperature-dependent plasticity with Damage option ";
    }
    else
    {
        of  << "\n Zerilli - Armstrong rate- and temperature-dependent plasticity ";
    }
    of << "\n   Young modulus:  E      : " << E
    << "\n   Poisson ratio:  nu     : " << nu
    << "\n   Lame constants: Lambda : " << lambda
    << "\n                   Mu     : " << mu
    << "\n   Bulk modulus:   K      : " << bulk
    << "\n   Density                : " << rho;
    if (rho > 0.0)
    {
        of  << "\n   Wave velocities C_p    : " << cp;
        of  << "\n                   C_s    : " << cs;
    }
    if (damageModel)
    {
    of  << "\n   The yield Kirchhoff stress is of the form:"
    << "\n    |tau| - (1-D) sqrt(2/3) (C1 + C2 eps^(1/2)) exp(-C3T + C4T ln(epsdot))+ C5eps^n+kl^(-1/2)+sigmaG";
    }
    else
    {
    of  << "\n   The yield Kirchhoff stress is of the form:"
    << "\n    |tau| - sqrt(2/3) (C1 + C2 eps^(1/2)) exp(-C3T + C4T ln(epsdot))+ C5eps^n+kl^(-1/2)+sigmaG";
    }
    
    of  << "\n   C0                  : " << _C0
    << "\n   C1                      : " << _C1
    << "\n   C2                      : " << _C2
    << "\n   C3                      : " << _C3
    << "\n   C4                      : " << _C4
    << "\n   C5                      : " << _C5
    << "\n   temp                    : " << _curT
    << "\n   n                       : " << _n;
    if (damageModel)
    {
        of  <<"\n  D1                : " << _D1
        <<"\n  D2                    : " << _D2
        <<"\n  D3                    : " << _D3
        <<"\n  D4                    : " << _D4
        <<"\n  D5                    : " << _D5
        <<"\n  Dmax                  : " << _Dmax
        <<"\n  T0                    : " << _refT
        <<"\n  Tm                    : " << _meltT
        <<"\n  dot{eps}_0            : " << _edot0;
    }
}




void zerilliArmstrongMaterial :: setRandom()
{
    E      = muesli::randomUniform(1.0, 10.0);
    nu     = muesli::randomUniform(0.05, 0.45);
    rho    = muesli::randomUniform(1.0, 100.0);
    _C0    = muesli::randomUniform(1.0, 10.0);
    _C1    = muesli::randomUniform(1.0, 10.0);
    _C2    = muesli::randomUniform(1.0, 10.0);
    _C3    = muesli::randomUniform(1.0, 10.0);
    _C4    = muesli::randomUniform(1.0, 10.0);
    _C5    = muesli::randomUniform(1.0, 10.0);
    _curT  = muesli::randomUniform(60.0, 80.0);
    _n     = muesli::randomUniform(60.0, 80.0);
    
    lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
    mu     = E/2.0/(1.0+nu);
    cp     = sqrt((lambda+2.0*mu)/rho);
    cs     = sqrt(2.0*mu/rho);
    bulk   = lambda + 2.0/3.0 * mu;
    
    if (damageModel)
    {
    _D1    = muesli::randomUniform(0.0, 10.0);
    _D2    = muesli::randomUniform(0.0, 10.0);
    _D3    = muesli::randomUniform(0.0, 10.0);
    _D4    = muesli::randomUniform(0.0, 10.0);
    _D5    = muesli::randomUniform(0.0, 10.0);
    _Dmax  = muesli::randomUniform(0.0, 1.0);
    _edot0 = muesli::randomUniform(1.0, 10.0);
    _meltT = muesli::randomUniform(400.0, 2000.0);
    _refT  = muesli::randomUniform(200.0, 500.0);
    }
}




bool zerilliArmstrongMaterial::test(std::ostream &of)
{
    bool isok=true;
    setRandom();
    
    muesli::finiteStrainMP* p = this->createMaterialPoint();
    p->setRandom();
    
    isok = p->testImplementation(of);
    delete p;
    return isok;
}




double zerilliArmstrongMaterial::waveVelocity() const
{
    return cp;
}




zerilliArmstrongMP::zerilliArmstrongMP(const zerilliArmstrongMaterial &m)
:
finiteStrainMP(m),
theElastoplasticMaterial(m),
iso_n(0.0), iso_c(0.0),
epdot_n(0.0), epdot_c(0.0),
dgamma(0.0)
{
    be_n = be_c = istensor::identity();
    tau.setZero();
    lambda2 = lambda2TR = ivector(1.0, 1.0, 1.0);
    tau.setZero();
    nubarTR.setZero();
    nn[0] = ivector(1.0, 0.0, 0.0);
    nn[1] = ivector(0.0, 1.0, 0.0);
    nn[2] = ivector(0.0, 0.0, 1.0);
}




// Brent method function
double zerilliArmstrongMP::brentroot(double a, double b, double Ga,
                                       double Gb, double eqpn, double ntbar,
                                       double mu, double C0,double C1,
                                       double C2, double C3,double C4,
                                       double C5, double Tc, double n, double dt)
{
    double s(0.0);
    double d(0.0);
    size_t cuenta=1;
    
    if (fabs(Ga) < fabs(Gb))
    {
        double ch=a;
        a=b; b=ch;
        double Gch=Ga;
        Ga=Gb; Gb=Gch;
    }
    
    double c = a;
    double Gs = Gb;
    double Gc = Ga;
    int flag=1;
    
    
    while( !(Gs == 0.0 ) && !((fabs(Ga) < J2TOL2) && (fabs(Gb) < J2TOL2))  && cuenta < GAMMAITER2)
    {
        if((Ga != Gc) && (Gb != Gc))
        {
            s=((a*Gb*Gc/((Ga-Gb)*(Ga-Gc)))+(b*Ga*Gc/((Gb-Ga)*(Gb-Gc)))+(c*Ga*Gb/((Gc-Ga)*(Gc-Gb))));
        }
        else
        {
            s=b-(Gb*((b-a)/(Gb-Ga)));
        }
        
        if(!(((3.0*a+b)/4.0)<s<b) || ((flag==1) && (fabs(s-b)>=fabs((b-c)/2.0))) || ((flag==0) && (fabs(s-b)>= fabs((c-d)/2.0))) || ((flag==1) && (fabs(b-c)< J2TOL2)) || ((flag==0) && (fabs(c-d)< J2TOL2)))
        {
            s=(a+b)/2.0;
            flag=1;
        }
        else
        {
            flag=0;
        }
        
        plasticReturnResidual(mu, C0, C1, C2, C3, C4, C5, Tc, n, eqpn, ntbar, dt, s, Gs);
        d=c;
        c=b;
        Gc=Gb;
        
        if(Ga*Gs < 0.0)
        {
            b=s;
            Gb=Gs;
        }
        else
        {
            a=s;
            Ga=Gs;
        }
        
        if (fabs(Ga) < fabs(Gb))
        {
            double cp=a;
            a=b; b=cp;
            double Gcp=Ga;
            Ga=Gb; Gb=Gcp;
        }
        
        cuenta++;
        
    }
    
    if (fabs(Ga) > J2TOL2 || fabs(Gb) > J2TOL2)
    {
        return b;
    }
    return s;
}




void zerilliArmstrongMP::CauchyStress(istensor& sigma) const
{
    const double iJ = 1.0/Jc;
    
    // Reconstruct Cauchy stress
    sigma.setZero();
    for (size_t i=0; i<3; i++)
    {
        sigma.addScaledVdyadicV(iJ * tau[i], nn[i]);
    }
}




void zerilliArmstrongMP::commitCurrentState()
{

    finiteStrainMP::commitCurrentState();
    
    be_n    = be_c;
    iso_n   = iso_c;
    epdot_n = epdot_c;
}




void zerilliArmstrongMP::contractWithConvectedTangent(const ivector& V1, const ivector& V2, itensor& T) const
{
    itensor4 c;
    convectedTangent(c);
    
    T.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    T(i,k) += c(i,j,k,l)*V1(j)*V2(l);
                }
}




void zerilliArmstrongMP :: convectedTangent(itensor4& C) const
{
    this->numericalConvectedTangent(C);
}




void zerilliArmstrongMP::convectedTangentTimesSymmetricTensor(const istensor& M,istensor& CM) const
{
    itensor4 C;
    convectedTangent(C);
    
    CM.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    CM(i,j) += C(i,j,k,l)*M(k,l);
                }
}




void zerilliArmstrongMP::spatialTangent(itensor4& C) const
{
    C.setZero();
    
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    if (i==j && k==l) C(i,j,k,l) += theElastoplasticMaterial.lambda;
                    if (i==k && j==l) C(i,j,k,l) += theElastoplasticMaterial.mu;
                    if (i==l && j==k) C(i,j,k,l) += theElastoplasticMaterial.mu;
                }
}




// Calculation of damage variable for each timestep
double zerilliArmstrongMP::damageCalc(double thet, ivector vone, double eps, double epsdot)
{
    const double D1         = theElastoplasticMaterial._D1;
    const double D2         = theElastoplasticMaterial._D2;
    const double D3         = theElastoplasticMaterial._D3;
    const double D4         = theElastoplasticMaterial._D4;
    const double D5         = theElastoplasticMaterial._D5;
    const double Tc         = theElastoplasticMaterial._curT;
    const double T0         = theElastoplasticMaterial._refT;
    const double Tm         = theElastoplasticMaterial._meltT;
    const double tempterm   = (Tc-T0)/(Tm-T0);
    const double edot0      = theElastoplasticMaterial._edot0;
    double epsf;
    istensor sigma;
    tau = tau/(1-Dn);
    CauchyStress(sigma);
    tau = (1-Dn)*tau;
    
    double hydro = (sigma(0,0)+sigma(1,1)+sigma(2,2))/3;
    double VMS=sqrt((((sigma(0,0)-sigma(1,1))*(sigma(0,0)-sigma(1,1))
                     +(sigma(1,1)-sigma(2,2))*(sigma(1,1)-sigma(2,2))
                     +(sigma(2,2)-sigma(0,0))*(sigma(2,2)-sigma(0,0)))/2)
                     +3*((sigma(0,1)*sigma(0,1))+(sigma(1,2)*sigma(1,2))+(sigma(2,0)*sigma(2,0))));
    triax = hydro/VMS;
    if (hydro==0.0 || std::isnan(triax))
    {
        triax = 0.0;
    }
    if (triax<1.5)
    {
        epsf = (D1+D2*exp(D3*triax))*(1+D4*log(epsdot/edot0))*(1+D5*tempterm);
    }
    else
    {
        epsf = (D1+D2*exp(D3*1.5))*(1+D4*log(epsdot/edot0))*(1+D5*tempterm);
    }
    double D = Dn+((iso_c-iso_n)/epsf);
    
    return D;
}




double zerilliArmstrongMP::deviatoricEnergy() const
{
    return 0.0;
}




double zerilliArmstrongMP::dissipatedEnergy() const
{
    double dissEnergy;
    const ivector vone(1.0, 1.0, 1.0);
    ivector taubar = tau - (1.0/3.0) * tau.dot(vone)*vone;
    if(tc-tn>0.0)
    {
        dissEnergy = dgamma*taubar.norm()/(tc-tn);
    }
    else
    {
        dissEnergy = 0.0;
    }
    return dissEnergy;
}




double zerilliArmstrongMP::effectiveStoredEnergy() const
{
    double dt = tc - tn;
    return storedEnergy() + dt * dissipatedEnergy();
}




// Explicit radial return algorithm not tested within this code
// and not used in the "update state" function
void zerilliArmstrongMP::explicitRadialReturn(const ivector &taudev, double ep, double epdot)
{
    const double mu  = theElastoplasticMaterial.mu;
    const double C0  = theElastoplasticMaterial._C0;
    const double C1  = theElastoplasticMaterial._C1;
    const double C2  = theElastoplasticMaterial._C2;
    const double C3  = theElastoplasticMaterial._C3;
    const double C4  = theElastoplasticMaterial._C4;
    const double C5  = theElastoplasticMaterial._C5;
    const double Tc  = theElastoplasticMaterial._curT;
    const double n   = theElastoplasticMaterial._n;
    
    double thermal(0.0),dthermal(1.0);
    
    if(epdot>0.0)
    {
        thermal  = (C1+C2*sqrt(ep))*exp(-C3*Tc+C4*Tc*log(epdot));
    }
    
    // Depending on the nature of the material (bcc or fcc structure), the algorithm selects
    // the terms to calculate the sigma fluent value
    if (C1==0.0 && epdot > 0.0)
    {
        dthermal = ((SQ23*C2/2.0)*(1.0/sqrt(ep))*exp(-C3*Tc+C4*Tc*log(epdot)))+((C2*sqrt(ep)*exp(-C3*Tc+C4*Tc*log(epdot)))*C4*Tc/epdot);
        double sigma_y = SQ23 * (thermal + C0);
        dgamma = (taudev.norm() - sigma_y)/(2.0*mu);
        iso_c = ep + SQ23*dgamma*dthermal;
    }
    
    else if (C2==0.0 && epdot > 0.0)
    {
        dthermal = C1*exp(-C3*Tc+C4*Tc*log(epdot))*C4*Tc/epdot;
        double strain  = C5*pow(ep,n);
        double dstrain = SQ23*n*C5*pow(ep,n-1.0);
        double sigma_y = SQ23 * (thermal + strain + C0);
        dgamma = (taudev.norm() - sigma_y)/(2.0*mu);
        iso_c = ep + SQ23*dgamma*(dthermal + dstrain);
    }
    
    else if(C1==0.0 && epdot==0.0)
    {
        dthermal=1.0;
        double sigma_y = SQ23 * (thermal + C0);
        dgamma = (taudev.norm() - sigma_y)/(2.0*mu);
        iso_c = ep + SQ23*dgamma*dthermal;
    }
    
    else if(C2==0.0 && epdot==0.0)
    {
        dthermal=0.0;
        double strain  = C5*pow(ep,n);
        double dstrain = SQ23*n*C5*pow(ep,n-1.0);
        double sigma_y = SQ23 * (thermal + strain + C0);
        dgamma = (taudev.norm() - sigma_y)/(2.0*mu);
        iso_c = ep + SQ23*dgamma*(dthermal + dstrain);
    }
}




double zerilliArmstrongMP::kineticPotential() const
{
    return 0.0;
}




double zerilliArmstrongMP::plasticSlip() const
{
    return iso_c;
}




// TaubarTR: trial deviatoric tau.
// This function finds dgamma root and equivalent plastic strain eqp in the current time step.
// Two-fold approach convergence algorithm, using in first place a NR scheme to find the root (dgamma)
// of G equation. In case this algorithm is not able (due to high derivative problems near the root
// associated to ZA equation), the function jumps to a Brent scheme, whose convergence is assured.
void zerilliArmstrongMP::plasticReturn(const ivector& taubarTR)
{
    const double mu      = theElastoplasticMaterial.mu;
    const double C0      = theElastoplasticMaterial._C0;
    const double C1      = theElastoplasticMaterial._C1;
    const double C2      = theElastoplasticMaterial._C2;
    const double C3      = theElastoplasticMaterial._C3;
    const double C4      = theElastoplasticMaterial._C4;
    const double C5      = theElastoplasticMaterial._C5;
    const double Tc      = theElastoplasticMaterial._curT;
    const double n       = theElastoplasticMaterial._n;
    const double dt      = tc - tn;
    
    
    double G, DG;
    double eqpn     = iso_n;
    double ntbar    = taubarTR.norm();
    int flagbrent   = 0;
    int flagbrent2  = 1;
    double x        = dt;
    
    plasticReturnResidual(mu, C0, C1, C2, C3, C4, C5, Tc, n, eqpn, ntbar, dt, x, G);
    
    
    // NR scheme. "x" represents the unknown delta-gamma
    size_t count = 0;
    while ( fabs(G) > J2TOL1 && ++count < GAMMAITER1 && x>0.0)
    {
        plasticReturnTangent(mu, C0, C1, C2, C3, C4, C5, Tc, n, eqpn, ntbar, dt, x, DG);
        x -= G/DG;
        plasticReturnResidual(mu, C0, C1, C2, C3, C4, C5, Tc, n, eqpn, ntbar, dt, x, G);
        
    }
    
    // The "if" condition tests whether the convergence with NR scheme has succeded or not, in which case
    // will set flagbrent to 1.
    if (count >= GAMMAITER1 || std::isnan(G) || x<=0.0)
    {
        flagbrent=1;
    }
    else
    {
        dgamma = x;
        iso_c  = iso_n + SQ23*dgamma;
    }
    
    // Initially, two points with different function values wihtin G must be given to the Brent method
    // to assure its convergence. Ga and Gb are both multiplied/divided until their sign is opposite.
    if(flagbrent==1)
    {
        eqpn = iso_n;
        ntbar = taubarTR.norm();
        double a=1e-12, b=1e-5;
        double Ga, Gb;
        
        plasticReturnResidual(mu, C0, C1, C2, C3, C4, C5, Tc, n, eqpn, ntbar, dt, a, Ga);
        plasticReturnResidual(mu, C0, C1, C2, C3, C4, C5, Tc, n, eqpn, ntbar, dt, b, Gb);
        
        while(Gb<0.0)
        {
            b=b*100.0;
            plasticReturnResidual(mu, C0, C1, C2, C3, C4, C5, Tc, n, eqpn, ntbar, dt, b, Gb);
            if (b>2.0) break;
        }
        
        // The value of "a" can be very close to zero due to the Zerilli-Armstrong equation features
        // and because of this the division loop is stopped, making not a big difference compared
        // to the real value, given that it is really small.
        while(Ga>0.0)
        {
            a=a/150.0;
            plasticReturnResidual(mu, C0, C1, C2, C3, C4, C5, Tc, n, eqpn, ntbar, dt, a, Ga);
            if (a<1e-50)
            {
                dgamma = 1e-50;
                flagbrent2 = 0;
                break;
            }
        }
        
        if(flagbrent == 1 && flagbrent2 == 1)
        {
            // Call to Brent method function in case NR fails and "a" value is high enough
            dgamma = brentroot(a, b, Ga, Gb, eqpn, ntbar, mu, C0, C1, C2, C3, C4, C5, Tc, n, dt);
            iso_c  = iso_n + SQ23*dgamma;
        }
    }
}




// Calculation of G function value given each iteration value of dgamma
void zerilliArmstrongMP::plasticReturnResidual(double mu, double C0, double C1, double C2, double C3,
                                                 double C4, double C5, double Tc, double n, double eqpn,
                                                 double tau, double dt, double dgamma, double& G)
{
    double deqp = SQ23*dgamma;
    double eqp  = eqpn + deqp;

    double thermal (0.0);
    if(deqp > 0.0 && dt > 0.0)
    {
        thermal = (C1+C2*sqrt(eqp))*exp(-C3*Tc+C4*Tc*log(deqp/dt));
    }
    
    if(C5!=0.0)
    {
        double strain  = C5*pow(eqp,n);
        G = 2.0*mu*dgamma - tau + SQ23*(thermal+strain+C0);
    }
    
    if(C5==0.0)
    {
        G = 2.0*mu*dgamma - tau + SQ23*(thermal+C0);
    }
}



// Calculation of G function derivative value given each iteration value of dgamma
void  zerilliArmstrongMP::plasticReturnTangent(double mu, double C0, double C1,double C2, double C3,
                                                 double C4, double C5, double Tc, double n, double eqpn,
                                                 double tau, double dt, double dgamma, double& DG)
{
    double deqp = SQ23*dgamma;
    double eqp  = eqpn + deqp;
    
    if(C1 == 0.0 && dgamma > 0.0 && dt > 0.0)
    {
        double dthermal = ((SQ23*C2/2.0)*(1.0/sqrt(eqp))*exp((-C3*Tc)+(C4*Tc*log(deqp/dt))))+((C2*sqrt(eqp)*exp((-C3*Tc)+(C4*Tc*log(deqp/dt))))*(C4*Tc/dgamma));
        DG = 2.0*mu + SQ23*dthermal;
    }
    
    else if(C2 == 0.0 && dgamma > 0.0 && dt > 0.0)
    {
        double dthermal = C1*exp(-C3*Tc+C4*Tc*log(deqp/dt))*C4*Tc/dgamma;
        double dstrain = SQ23*n*C5*pow(eqp,n-1.0);
        DG = 2.0*mu + SQ23*(dthermal + dstrain);
    }
    
    else if(C1 == 0.0 && dgamma == 0.0)
    {
        double dthermal = 1.0;
        DG = 2.0*mu + SQ23*dthermal;
    }
    
    else if(C2 == 0.0 && dgamma == 0.0)
    {
        double dthermal = 1.0;
        double dstrain = SQ23*n*C5*pow(eqp,n-1.0);
        DG = 2.0*mu + SQ23*(dthermal + dstrain);
    }
}




void zerilliArmstrongMP::resetCurrentState()
{
    finiteStrainMP::resetCurrentState();
    
    iso_c   = iso_n;
    be_c    = be_n;
    epdot_c = epdot_n;
}




void zerilliArmstrongMP::setConvergedState(const double theTime, const itensor& F,
                                             const double iso, const ivector& kine,
                                             const istensor& be)
{
    tn     = theTime;
    Fn     = F;
    Jn     = Fn.determinant();
    iso_n  = iso;
    be_n   = be;
}



void zerilliArmstrongMP::setRandom()
{
    iso_c = iso_n = muesli::randomUniform(1.0, 2.0);
    
    ivector tmp; tmp.setRandom();
    ivector vone(1.0, 1.0, 1.0);
    
    itensor Fe; Fe.setRandom();
    if (Fe.determinant() < 0.0) Fe *= -1.0;
    be_c = be_n = istensor::tensorTimesTensorTransposed(Fe);
    
    itensor Fp; Fp.setRandom();
    Fp *= 1.0/cbrt(Fp.determinant());
    Fc = Fn = Fe*Fp;
    Jc = Fc.determinant();
    
    if (theElastoplasticMaterial.damageModel)
    {
        Dc = muesli::randomUniform(0.0, 1.0);
    }
}




double zerilliArmstrongMP::storedEnergy() const
{
    const double lambda = theElastoplasticMaterial.lambda;
    const double mu     = theElastoplasticMaterial.mu;
    
    
    // Logarithmic principal elastic stretches
    ivector lambda2, xeigvec[3];
    be_c.spectralDecomposition(xeigvec, lambda2);
    
    ivector epse;
    for (size_t i=0; i<3; i++)
    {
        epse[i] = 0.5*log(lambda2[i]);
    }
    
    const double We = 0.5*lambda*( epse(0) + epse(1) + epse(2) ) * ( epse(0) + epse(1) + epse(2) )
    + mu * epse.squaredNorm();
    double Wp = dissipatedEnergy()*0.1/0.9;
    return Wp+We;
}




void zerilliArmstrongMP::updateCurrentState(const double theTime, const istensor& C)
{
    itensor U = istensor::squareRoot(C);
    this->updateCurrentState(theTime, U);
}




void zerilliArmstrongMP::updateCurrentState(const double theTime, const itensor& F)
{
    finiteStrainMP::updateCurrentState(theTime, F);
    
    
    // Recover material parameters
    const double mu     = theElastoplasticMaterial.mu;
    const double kappa  = theElastoplasticMaterial.bulk;
    const ivector vone(1.0, 1.0, 1.0);
    int flagD = 1;
    int flagDc = 0;
    
    // Incremental deformation gradient f = F * Fn^-1 */
    itensor f = F * Fn.inverse();
    
    
    //---------------------------------------------------------------------------------------
    //                                     trial state
    //---------------------------------------------------------------------------------------
    // Trial elastic finger tensor b_e
    istensor beTR = istensor::FSFt(f, be_n);
    
    // Logarithmic principal elastic stretches
    beTR.spectralDecomposition(nn, lambda2TR);
    ivector epseTR;
    ivector devEpseTR;
    for (size_t i=0; i<3; i++)
    {
        epseTR[i] = 0.5*log(lambda2TR[i]);
    }
    double theta = epseTR.dot(vone);
    
    // Trial state with frozen plastic flow
    if (!theElastoplasticMaterial.damageModel || (theElastoplasticMaterial.damageModel && Dn!=1.0))
    {
        devEpseTR = epseTR - 1.0/3.0*theta*vone;
    }
    double isoTR = iso_n;
    double  DTR       = Dn;
    ivector tauVol    = kappa*theta*vone;
    ivector tauTR;
    
    // Trial deviatoric principal Kirchhoff strees -- Hencky model
    if (theElastoplasticMaterial.damageModel && Dn==1.0)
    {
        tauTR = tauVol;
        if(theta>0.0)
        {
            tauVol[0]=tauVol[1]=tauVol[2]=0.0;
        }
    }
    else
    {
        tauTR = tauVol + 2.0*mu*devEpseTR;
    }
    
    // Yield function value at trial state
    double phiTR;
    if(theElastoplasticMaterial.damageModel)
    {
        phiTR = yieldFunctionDMG(tauTR, isoTR, 0.0, DTR);
    }
    else
    {
        phiTR = yieldFunction(tauTR, isoTR, 0.0);
    }
    
    // Explicit computations, deactivated in this version
    //double edot0 = theElastoplasticMaterial._edot0;
    //double edot = (epdot_n < 1e-4*edot0)  ? 1e-4*edot0 : epdot_n;
    //double phiTR = yieldFunction(tauTR, iso_n, edot);
    
    //---------------------------------------------------------------------------------------
    //                   check trial state and radial return
    //---------------------------------------------------------------------------------------
    ivector devepse_c;
    
    if (phiTR <= 0.0)
    {
        // Elastic step: trial -> n+1
        iso_c     = isoTR;
        devepse_c = devEpseTR;
        dgamma    = 0.0;
        flagD = 0;
        if(theElastoplasticMaterial.damageModel)
        {
            Dc = DTR;
        }
    }
    else
    {
        if (!theElastoplasticMaterial.damageModel || Dn!=1.0)
        {
            // Deviatoric stress
            devTauTR = 2.0*mu*devEpseTR;
        
            // Plastic step : return mapping in principal stretches space,
            // solution goes into MP variables dgamma and iso_c
            plasticReturn(devTauTR);
        
            //explicitRadialReturn(devTauTR, iso_n, edot);
        
            // Correct the trial quantities
            nubarTR = devTauTR * (1.0/devTauTR.norm());
            devepse_c = devEpseTR - dgamma * nubarTR;
        }
    }
    
    // Update elastic finger tensor
    ivector epse_c;
    if (theElastoplasticMaterial.damageModel && Dn==1.0)
    {
        epse_c = 1.0/3.0*theta*vone;
    }
    else
    {
        epse_c = devepse_c + 1.0/3.0*theta*vone;
    }
    
    be_c.setZero();
    for (unsigned i=0; i<3; i++)
    {
        lambda2(i) = exp(2.0*epse_c[i]);
        be_c.addScaledVdyadicV( lambda2(i), nn[i] );
    }
    
    double theta_c = epse_c.dot(vone);
    // Update of Kirchhof stress and Damage variable, in case Damage model is in use
    // and the temporal step lies within the plastic regime deformation
    if(theElastoplasticMaterial.damageModel && Dn!=1.0)
    {
        if (theta_c>0.0)
        {
            tau = (1.0-Dn) * (tauVol + 2.0 * mu * devepse_c);
        }
        else
        {
            tau =  tauVol + (1.0-Dn) * 2.0 * mu * devepse_c;
        }
    }
    else if(theElastoplasticMaterial.damageModel && Dn==1.0)
    {
        if (theta_c>0.0)
        {
            tau[0]=tau[1]=tau[2]=0.0;
        }
        else
        {
            tau =  tauVol;
        }
    }
    else if (!theElastoplasticMaterial.damageModel)
    {
        tau = tauVol + 2.0 * mu * devepse_c;
    }
    
    double dt = tc - tn;
    if(dgamma>0.0)
    {
        epdot_c = dt > 0.0  ? SQ23*dgamma/dt : epdot_n;
    }
    else
    {
        epdot_c = dt > 0.0 ? SQ23*1e-50/dt : epdot_n;
    }
    
    if(theElastoplasticMaterial.damageModel && flagD==1 && fullyDamaged==false)
    {
        Dc = damageCalc(theta, vone, iso_c, epdot_c);
        if(Dc>theElastoplasticMaterial._Dmax)
        {
            Dc = theElastoplasticMaterial._Dmax;
            if (std::isnan(Dc))
            {
                Dc=Dn;
            }
            fullyDamaged = true;
            if (theta_c>0.0)
            {
                tau = (1.0-Dc) * (tauVol + 2.0 * mu * devepse_c);
            }
            else
            {
                tau =  tauVol + (1.0-Dc) * 2.0 * mu * devepse_c;
            }
            flagDc = 1;
        }
    }
    
    //Testing of yield function to test dgamma root solution, deactivated by default
    /*if(theElastoplasticMaterial.damageModel)
    {
        if (flagDc == 0)
        {
            double kk = yieldFunctionDMG(tau, iso_c, epdot_c, Dn);
            if (fabs(kk)>0.1 && dgamma>0.0)
            {
                std::cout<<"\n"<<kk;
                std::cout<<"\n"<<dgamma;
                std::cout<<"\n"<<theTime<<"\n";
            }
        }
        if (flagDc == 1)
        {
            double kk = yieldFunctionDMG(tau, iso_c, epdot_c, Dc);
            if (fabs(kk)>0.1 && dgamma>0.0)
            {
                std::cout<<"\n"<<kk;
                std::cout<<"\n"<<dgamma;
                std::cout<<"\n"<<theTime<<"\n";
            }
        }
    }
    else
    {
        double kk = yieldFunction(tau, iso_c, epdot_c);
        if (fabs(kk)>0.1 && dgamma>0.0)
        {
            std::cout<<"\n"<<kk;
            std::cout<<"\n"<<dgamma;
        }
    }*/
}




double zerilliArmstrongMP::volumetricEnergy() const
{
    return 0.0;
}




// Yield function in principal Kirchhoff space
double zerilliArmstrongMP::yieldFunction(const ivector& tau,
                                           const double&  eps,
                                           const double&  epsdot) const
{
    const double C0 = theElastoplasticMaterial._C0;
    const double C1 = theElastoplasticMaterial._C1;
    const double C2 = theElastoplasticMaterial._C2;
    const double C3 = theElastoplasticMaterial._C3;
    const double C4 = theElastoplasticMaterial._C4;
    const double C5 = theElastoplasticMaterial._C5;
    const double Tc = theElastoplasticMaterial._curT;
    const double n  = theElastoplasticMaterial._n;
    
    
    const ivector vone(1.0, 1.0, 1.0);
    double thermal = (epsdot>0.0) ? (C1+C2*sqrt(eps))*exp(-C3*Tc+C4*Tc*log(epsdot)) : 0.0;
    
    if(C5!=0.0)
    {
        double strain  = C5*pow(eps,n);
        double sigma_y = SQ23 * (thermal + strain + C0);
        ivector taubar = tau - (1.0/3.0) * tau.dot(vone)*vone;
        return  taubar.norm() - sigma_y;
    }
    else
    {
        double sigma_y = SQ23 * (thermal + C0);
        ivector taubar = tau - (1.0/3.0) * tau.dot(vone)*vone;
        return  taubar.norm() - sigma_y;
    }
}




// Yield function in principal Kirchhoff space, when Damage model is in use
double zerilliArmstrongMP :: yieldFunctionDMG(const ivector& tau,
                                           const double&  eps,
                                           const double&  epsdot,
                                           const double& D) const
{
    const double C0 = theElastoplasticMaterial._C0;
    const double C1 = theElastoplasticMaterial._C1;
    const double C2 = theElastoplasticMaterial._C2;
    const double C3 = theElastoplasticMaterial._C3;
    const double C4 = theElastoplasticMaterial._C4;
    const double C5 = theElastoplasticMaterial._C5;
    const double Tc = theElastoplasticMaterial._curT;
    const double n  = theElastoplasticMaterial._n;
    
    
    const ivector vone(1.0, 1.0, 1.0);
    double thermal = (epsdot>0.0) ? (C1+C2*sqrt(eps))*exp(-C3*Tc+C4*Tc*log(epsdot)) : 0.0;

    if(C5!=0.0)
    {
        double strain  = C5*pow(eps,n);
        double sigma_y = (1-D)* SQ23 * (thermal + strain + C0);
        ivector taubar = tau - (1.0/3.0) * tau.dot(vone)*vone;
        return  taubar.norm() - sigma_y;
    }
    else
    {
        double sigma_y = (1-D)* SQ23 * (thermal + C0);
        ivector taubar = tau - (1.0/3.0) * tau.dot(vone)*vone;
        return  taubar.norm() - sigma_y;
    }
}
